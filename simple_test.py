#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 12:27:28 2018

@author: btsween
"""
from pycsa.CEPAlgorithms import MSAAlgorithms
import pickle
import ctypes
import numpy
from numpy.ctypeslib import ndpointer


if __name__ == '__main__':
    aln_file = 'senior-design-2017/pycsa/tests/pdz_test.aln'
    gap_cutoff = 1.0
    percent_change = 0.0
    pc_type = 'inc'
    pc_mix = 0.5
    swt_method='henikoff'
    
    msa = MSAAlgorithms(alnFile=aln_file,gapFreqCutoff=gap_cutoff,percentChange=percent_change,
                        pcType=pc_type,pcMix=pc_mix,swtMethod=swt_method)
    msa.calculate_mutual_information()
    
    
    symbolCounts = ctypes.CDLL('/Users/btsween/Documents/Scratch/seniorDesign/src/countLib.dylib')
    symbolCounts.convertChars.argtypes = (ndpointer(ctypes.c_int),ctypes.c_int,ndpointer(ctypes.c_char))
    
    
    symbols = numpy.array([])
    for x in range(len(msa.columns)):
        if (x==0):
            symbols=numpy.asarray(msa.columns[x])
        else:
            symbols = numpy.append(symbols,numpy.asarray(msa.columns[x]))
    numberArray = numpy.zeros(len(symbols),dtype=numpy.int32)
    symbolCounts.convertChars(numberArray,len(numberArray),symbols)
    
    print numberArray
    
    
    
    
    '''
    These commands used to pickle the msa object
    msaPickle = "testFile"
    fileObject = open(msaPickle,'wb')
    pickle.dump(msa,fileObject)
    fileObject.close()
    '''