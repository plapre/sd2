/*
 ============================================================================
 Name        : seniorDesign.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int count(int *col,int lenCol, int *count){
	int i = 0;
	while (i<lenCol){

		//a will be index so remove 1 from actual value since start at 1
		int a = *(col+i) - 1;

		//adds 1 to count
		*(count+a) = *(count+a) + 1;
		i++;
	}
	return 0;
}

int countM(int *array,int lenCol, int *count){
	int i = 0;
	int j=0;
	/*
	 * while less than the length of a column
	 * Want lower number to be the vertical and the higher to be the horizontal
	 * Think for tallying n0 is down and n1 is across:
	 *
	 *  ABC
	 * A
	 * B
	 * C
	 *
	 * For now only picture being fed a vertical array that represents 2 of the sequences appended
	 */

	while (i<lenCol){
		j=i+lenCol;
		int value0 = *(array+i);
		int value1 = *(array+j);

		//values as of now start at 1.... so val-1 gives index
		int loc0 = value0-1;
		int loc1 = value1-1;

		//creating a length** matrix techinically but will be all appended here so l*l long
		int index = loc0*lenCol+loc1;
		*(count+index)=*(count+index)+1;

		i++;
	}
	return 0;
}

int main(void) {

	int col0[] = {1,4,3,1};
	int col1[] = {3,2,2,1};
	int col2[] = {2,1,2,3};
	int col3[] = {1,1,1,1};
	
	int colsAppTest[] = {1,4,3,1,3,2,2,1};
	//colsAppTest is col0 and col1 appended, but each one is just 4 long so....
	int lenCol = 4;

	//This array is for the countM function, is techinically a matrix of two columns against e.o. but setup as appended arrays L** length
	int countApp[16] = {0};

	countM(colsAppTest,lenCol,countApp);


	//shows how to reach row,col of appended array

	/*int col = 1;
	int row = 0;
	int length = 4;
	int location = col*length+row;
	printf("%d \t", *(colsApp+location));
	*/

	
	int x =0;
	while (x<16){
				printf("%d ",*(countApp+x));
				x++;
	}

	int count0[4] = {0};
	int count1[4] = {0};
	int count2[4] = {0};
	int count3[4] = {0};
	
	count(col0,4,count0);
	count(col1,4,count1);
	count(col2,4,count2);
	count(col3,4,count3);

	return EXIT_SUCCESS;
}
