/*
 ============================================================================
 Name        : seniorDesign.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int count(int *col,int lenCol, int *count){
	int i = 0;
	while (i<lenCol){

		int a = *(col+i);

		//adds 1 to count
		*(count+a) = *(count+a) + 1;
		i++;
	}
	return 0;
}

int countM(int *array,int lenCol, int *count){
	int i = 0;
	int j=0;
	/*
	 * while less than the length of a column
	 * Want lower number to be the vertical and the higher to be the horizontal
	 * Think for tallying n0 is down and n1 is across:
	 *
	 *  ABC
	 * A
	 * B
	 * C
	 *
	 * For now only picture being fed a vertical array that represents 2 of the sequences appended
	 */

	while (i<lenCol){
		j=i+lenCol;
		int value0 = *(array+i);
		int value1 = *(array+j);

		//no :->    values as of now start at 1.... so val-1 gives index
		int loc0 = value0;
		int loc1 = value1;

		//creating a length** matrix techinically but will be all appended here so l*l long
		int index = loc0*lenCol+loc1;
		*(count+index)=*(count+index)+1;

		i++;
	}
	return 0;
}

int symCount(char symbol){
	int index;
	switch(symbol){
		case 'A':
		index = 0;
		break;
		case 'C':
		index = 1;
		break;
		case 'D':
		index = 2;
		break;
		case 'E':
		index = 3;
		break;
		case 'F':
		index = 4;
		break;
		case 'G':
		index = 5;
		break;
		case 'H':
		index = 6;
		break;
		case 'I':
		index = 7;
		break;
		case 'K':
		index = 8;
		break;
		case 'L':
		index = 9;
		break;
		case 'M':
		index = 10;
		break;
		case 'N':
		index = 11;
		break;
		case 'P':
		index = 12;
		break;
		case 'Q':
		index = 13;
		break;
		case 'R':
		index = 14;
		break;
		case 'S':
		index = 15;
		break;
		case 'T':
		index = 16;
		break;
		case 'V':
		index = 17;
		break;
		case 'W':
		index = 18;
		break;
		case 'Y':
		index = 19;
		break;
		case '-':
		index = 20;
		break;
	}


	return index;

}

int convertChars(int *col,int length,char *symbols){
	int i = 0;
	while(i<length){
		*(col+i)=symCount(*(symbols+i));
		i = i+1;
	}
	return 0;
}

int main(void) {

	int converted[4] ={0};
	char symbols[] = {'A','-','C','-'};
	convertChars(converted,4,symbols);
	for(int i=0;i<4;i++){
		printf("%d ",*(converted+i));
	}

	/*
	int col0[] = {0,1,4,3,1};
	int col1[] = {3,2,2,1,0};
	int col2[] = {4,1,2,4,0};
	int col3[] = {1,1,1,1,1};
	
	int colsAppTest[] = {1,4,3,1,3,2,2,1};
	//colsAppTest is col0 and col1 appended, but each one is just 5 long so....
	int lenCol = 5;

	//This array is for the countM function, is techinically a matrix of two columns against e.o. but setup as appended arrays L** length
	int countApp[25] = {0};

	countM(colsAppTest,lenCol,countApp);


	//shows how to reach row,col of appended array

	
	int x =0;
	while (x<25){
				printf("%d ",*(countApp+x));
				x++;
	}

	int count0[5] = {0};
	int count1[5] = {0};
	int count2[5] = {0};
	int count3[5] = {0};

	count(col0,5,count0);
	count(col1,5,count1);
	count(col2,5,count2);
	count(col3,5,count3);

	int a = 0;
	while (a<5){
		printf("%d",*(count0+a));
		a++;
	}
	*/

	return EXIT_SUCCESS;
}

