#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 10:43:30 2018

@author: btsween
"""
import ctypes
import numpy
from numpy.ctypeslib import ndpointer

array = numpy.array([3,2,3,1,4],dtype=numpy.int32)
countArray = numpy.array([0,0,0,0,0],dtype=numpy.int32)
symbolCounts = ctypes.CDLL('/Users/btsween/Documents/Scratch/seniorDesign/src/countLib.dylib')
symbolCounts.count.argtypes = (ndpointer(ctypes.c_int),ctypes.c_int,ndpointer(ctypes.c_int))
symbolCounts.count(array,ctypes.c_int(5),countArray)
print countArray